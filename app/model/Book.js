Ext.define('SimpleGridDemo.model.Book', {
    extend: 'Ext.data.Model',
    proxy:{
        type: 'ajax',
        reader: 'xml'
    },
    
    fields: [
        { name: 'Author', type: 'auto', mapping: '@author.name' },
        { name: 'Title', type: 'auto' },
        { name: 'Manufacturer', type: 'auto' },
        { name: 'ProductGroup', type: 'auto' },
        { name: 'DetailPageURL', type: 'auto' }

    ]
});
