
Ext.define("SimpleGridDemo.view.books.BookGrid",{
    extend: "Ext.grid.Panel",

    xtype: 'bookgrid',

    requires: [
        "SimpleGridDemo.view.books.BookGridController",
        // "SimpleGridDemo.view.books.BookGridModel"
    ],

    controller: "books-bookgrid",
    // viewModel: {
    //     type: "books-bookgrid"
    // },

    bufferedRenderer: false,
    store: 'BookStore',

    columns:[
        {text: 'Author', width: 120, dataIndex: 'Author', sortable: true},
        {text: 'Title', flex: 1, dataIndex: 'Title', sortable:true},
        {text: 'Manufacturer', width: 125, dataIndex: 'Manufacturer', sortable: true},
        {text: 'Product Group', width: 125, dataIndex: 'ProductGroup', sortable: true}
    ],

    forceFit: true,
    height: 210,
    split: true,
    region: 'north'

});
