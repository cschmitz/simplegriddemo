
Ext.define("SimpleGridDemo.view.book.BookPanel",{
    extend: "Ext.panel.Panel",
    xtype: 'bookdetails',
    // requires: [
    //     "SimpleGridDemo.view.book.BookPanelController",
    //     "SimpleGridDemo.view.book.BookPanelModel"
    // ],

    // controller: "book-bookpanel",
    // viewModel: {
    //     type: "book-bookpanel"
    // },
    region: 'center',
    bodyPadding: 7,
    bodyStyle: "Background: #ffffff",
    html: "Please select a book to see additional details."
});
