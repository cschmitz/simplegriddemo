/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('SimpleGridDemo.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'SimpleGridDemo.view.main.MainController',
        'SimpleGridDemo.view.main.MainModel',
        'SimpleGridDemo.view.books.BookGrid',
        "SimpleGridDemo.view.book.BookPanel"
    ],

    xtype: 'app-main',
    

    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'bookgrid'
        },
        {
            xtype: 'bookdetails'
        }
    ]
});
