Ext.define('SimpleGridDemo.store.BookStore',{
    extend: 'Ext.data.Store',
    model: 'SimpleGridDemo.model.Book',
    storeId: 'BookStore',

    autoLoad: true,
    proxy:{
        type: 'ajax',
        url: 'sheldon.xml',
        reader:{
            type: 'xml',
            record: 'Item',
            totalProperty: 'total'
        }
    }
});